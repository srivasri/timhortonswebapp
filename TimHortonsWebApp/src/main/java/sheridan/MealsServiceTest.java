package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		assertTrue("Invalid test", MealsService.getAvailableMealTypes(MealType.DRINKS).isEmpty() );
	}
	
	@Test
	public void testDrinksException() {
		List<String> list = MealsService.getAvailableMealTypes(null);
		assertTrue("No Brand Available",list.get(0).equalsIgnoreCase("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		assertFalse("No Brand Available", MealsService.getAvailableMealTypes(null).size()<0 );
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		assertTrue("No Brand Available", MealsService.getAvailableMealTypes(MealType.DRINKS).size()<2);
	}

}
